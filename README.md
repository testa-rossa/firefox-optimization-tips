# TUTORIAL FIREFOX

O Firefox é o navegador mais rápido e mais seguro, mas podemos deixá-lo ainda melhor:


**1. VERSÕES ALTERNATIVAS DO FIREFOX:**


- **[Firefox](https://www.mozilla.org/pt-BR/firefox/new/) –** versão normal.

- **[LibreWolf](https://librewolf.net/installation/windows/) –** Melhor versão alternativa do Firefox, customizada e independente com foco em privacidade e segurança.

- **[Tor Browser](https://www.torproject.org/pt-BR/download/) –** Navegador desenvolvido a partir do Firefox para garantir o anonimato ao usar a rede Tor, evita rastreamento, burla censura e esconde o IP.

- **[Mullvad Browser](https://mullvad.net/en/download/browser/windows) –** Desenvolvido a partir do Tor Browser, mas sem acesso à rede Tor, voltado para uso com VPN.


| Browser for privacy | Ad tracking prevention | Mobile support? | Onion routing? |
| ------------------- | ---------------------- | --------------- | -------------- |
|        Brave        |            ✓           |        ✓        |        ✓       |
|     DuckDuckGo      |            ✓           |        ✓        |    Available   |
|   Mozilla Firefox   |        Available       |        ✓        |        X       |
|        Brave        |            ✓           |        x        |    Via TOR ✓   |
|         Tor         |            ✓           |        ✓        |        ✓       |

Veja uma comparação com mais navegadores e mais detalhada em [privacytest.org](https://privacytests.org/).

**2. CONFIGURANDO O FIREFOX:**


Entre nas configurações do Firefox e configure da seguinte maneira para obter o máximo de segurança:


    1- GERAL:
    1.1- Marcar: Ativar abas contêiner;
    1.2- Desmarcar: Recomendar extensões enquanto você navega;
    1.3- Desmarcar: Recomendar recursos enquanto você navega;
    2- INICIO:
    2.1- Desmarcar Atalhos patrocinados;
    3- PESQUISA:
    3.1- Desmarcar: Mostrar sugestões de pesquisa (e todos subitens);
    4- PRIVACIDADE E SEGURANÇA:
    4.1- Em Proteção aprimorada contra rastreamento, selecionar: Rigoroso;
    4.2- Marcar: Dizer aos sites para não vender nem compartilhar meus dados;
    4.3- Marcar: Enviar aos sites uma solicitação de “Não rastrear”;
    4.4- Clicar em Permissões > Notificações > Configurações e marcar: Bloquear novas solicitações de permissão para exibir notificações;
    4.5- Marcar: Bloquear abertura de janelas ou abas;
    4.6- Em Coleta e uso de dados pelo Firefox: Demarcar tudo;
    4.7- Em Preferências de publicidade em sites: Demarcar tudo;
    4.8- Em Modo somente HTTPS, selecionar: Ativar o modo somente HTTPS em todas as janelas;
    4.9- Em Ativar DNS sobre HTTPS usando, selecionar: Proteção maior.


**3. EXTENSÕES DE SEGURANÇA:**


O uso de extensões ajuda a dar novas funções ao navegador e à melhorar a segurança. Mas não exagere para não prejudicar a velocidade e somente use extensões de fontes confiáveis, pois, eventualmente elas podem ter acesso aos seus dados.

- **[uBlock Origin](https://addons.mozilla.org/pt-BR/firefox/addon/ublock-origin/) -** Um AdBlocker é indispensável, pois ele bloqueia propagandas, endereços fontes de golpes e malwares, tracking (rastreamento / coleta de informações) e muitas outras coisas. Como resultado promove mais segurança, privacidade, velocidade de navegação (devido à desnecessidade de baixar os elementos bloqueados) e melhora a exibição de sites (devido aos anúncios bloqueados). Por exemplo, sabe aqueles sites cheios de botões de download enganosos quem levam o usuário a baixar coisas indesejadas, ou aqueles balões mirabolantes que atrapalham a navegação, ou aqueles anúncios de cookies, tudo isso simplesmente não aparece com um bom adBlock. Ainda permite ainda adicionar filtros específicos, como de pornografia, jogatina, fakenews, etc. O uBlock Origin se destaca como o melhor adblocker da atualidade, com um aparência simples, muitos recursos embutidos e é extremamente leve. Veja adiante como configurá-lo.
Semelhantes: AdGuard, Adblock Plus, Disconnect, Abine Blur, Ghostery, Adblocker Ultimate

- **[Privacy Possum](https://addons.mozilla.org/pt-BR/firefox/addon/privacy-possum/) -** Inibe métodos comuns de rastreamento comercial, reduzindo e falsificando os dados coletados por empresas de rastreamento. Considerado uma evolução do [Privacy Badger](https://addons.mozilla.org/pt-BR/firefox/addon/privacy-badger17/). Pode quebrar alguns sites.

- **[NoScript](https://addons.mozilla.org/pt-BR/firefox/addon/noscript/) -** Bloqueia a execução automática de todos os scripts (principalmente Java) e substitui por um ícone, onde basta clicar em cima para libera o carregamento do elemento. Além disso, fornece proteção contra ataques XSS, Clickjacking, proteção ABE, contra redirecionamentos META maliciosos e bloqueio XSLT. O bloqueio de scripts é uma das melhores ferramentas de proteção online, entretanto a maioria dos sites a utiliza, de maneira que a utilização desta extensão dificulta muito a navegação. Mas você pode desativá-la clicando com o botão direito do mouse e liberando o site ou marcando a opção “Desativar restrições globalmente”. O uBlock possui função semelhante.

- **[Google search link fix](https://addons.mozilla.org/pt-BR/firefox/addon/google-search-link-fix-mod/) -** O buscador Google sempre modifica os links dos resultados da pesquisa sem que você veja, para quando você clicar sobre eles, poderem registrar seus cliques (rastrear e colher informações). Esta extensão impede isto. Não é essencial e é melhor usar um buscador seguro alternativo (veja adiante). Semelhantes: [Don't track me Google ](https://addons.mozilla.org/pt-BR/firefox/addon/dont-track-me-google1/)

- **[Local CDN ](https://addons.mozilla.org/pt-BR/firefox/addon/localcdn-fork-of-decentraleyes/) -** Evita a participação de terceiros (CDNs) disponibilizando arquivos locais (embutidos no complemento) que carregam muito mais rápido e melhoram a privacidade online. Previne que muitas páginas da web façam requisições que comprometem sua privacidade. Decentraleyes ficou muito desatualizado. Uso de CDNs locais só funcionam com os scripts incluídos, mas não com outras conexões de terceiros, e podem aumentar a impressão digital. Após implementação do Total Cookie Protection no Firefox este tipo de extensão tornou-se desnecessária, basta ir nas configurações e definir a “Proteção aprimorada contra rastreamento” como Rigoroso.
Semelhantes: [Decentraleyes](https://addons.mozilla.org/pt-BR/firefox/addon/decentraleyes/)

- **[ClearURLs](https://addons.mozilla.org/pt-BR/firefox/addon/clearurls/) -** Limpa trechos da URL destinados a rastreamento. Se tornou desnecessária a partir do momento que o Ublock Origin adicionou o recurso “removeparam”. Basta adicionar a lista AdGuard URL Tracking (disponível nativamente) ou Legitimate URL Shortener (ver listas para adBlocks adiante). Semelhantes: [CleanLinks](https://addons.mozilla.org/pt-BR/firefox/addon/clean-links-webext/), [Neat URL](https://addons.mozilla.org/pt-BR/firefox/addon/neat-url/)

- **[Firefox Multi-Account Containers](https://addons.mozilla.org/pt-BR/firefox/addon/multi-account-containers/) -** Permite abrir os sites em grupos específicos (compras, Pessoal, Trabalho, Redes Sociais, Etc) onde os grupos não compartilham informações e cookies entre si, proporcionando maior segurança e privacidade. Tornou-se desnecessário depois da implementação do Total Cookie Protection no Firefox. Poderá ainda ter alguma utilidade se você usar o Mozilla VPN, pois será integrado, permitindo que você configure contêineres específicos para usar um servidor VPN específico. Outro uso pode ser se você quiser fazer login em várias contas no mesmo domínio.

- **[Temporary Containers](https://addons.mozilla.org/pt-BR/firefox/addon/temporary-containers/) -** Permite abrir sites e links em contêineres descartáveis gerenciados automaticamente que isolam os dados armazenados pelos sites (cookies, armazenamento e muito mais) uns dos outros, aumentando sua privacidade e segurança enquanto você navega.

- **[CanvasBlocker](https://addons.mozilla.org/pt-BR/firefox/addon/canvasblocker/) -** Impede que sites usem algumas APIs Javascript para fazer fingerprinting. Os usuários podem escolher bloquear as APIs inteiramente em alguns sites, em todos (pode quebrar alguns sites) ou falsificar sua API para criar uma fingerprinting falsa. O Firefox tem implementado uma função que deixará esta extensão inútil.

- **[Skip Redirect](https://addons.mozilla.org/pt-BR/firefox/addon/skip-redirect/) -** Algumas páginas usam páginas intermediárias antes de redirecionar para a página final. Este add-on tenta extrair a URL final da URL intermediária e ir para lá imediatamente.

- **[Smart Referer](https://addons.mozilla.org/pt-BR/firefox/addon/smart-referer/) -** Omite o cabeçalho Referer das solicitações HTTP(S), a menos que a solicitação seja para o mesmo domínio da página em que você já está. O cabeçalho Referer inclui o endereço da página em que você estava antes de navegar para a página atual. Isso pode ser usado para rastreamento. Os melhores padrões dos navegadores para sites HTTPS tornou essa extensão desnecessária.

**4. OUTRAS EXTENSÕES ÚTEIS:**

- **[Video Speed Controller](https://addons.mozilla.org/pt-BR/firefox/addon/videospeed/) -** Permite controlar a velocidade de reprodução de vídeos.

- **[Full Page Screenshot](https://addons.mozilla.org/pt-BR/firefox/addon/fullpage-screen-capture-plerdy/) -** Tira print da página inteira (como se estivesse rolando para baixo).

- **[Simple Translate](https://addons.mozilla.org/pt-BR/firefox/addon/simple-translate/) -** Traduz o texto selecionado exibindo em um balão flutuante rapidamente.

- **[Easy Youtube Video Downloader Express](https://addons.mozilla.org/pt-BR/firefox/addon/easy-youtube-video-download/) -** Adiciona um botão abaixo do player do Youtube permitindo baixar o vídeo ou o áudio facilmente, em diversas qualidades diferentes.

- **[Tampermonkey](https://addons.mozilla.org/pt-BR/firefox/addon/tampermonkey/) -** Permite instalar scripts a partir da internet que adicionam diversas funções ou personalizações em determinados sites da internet. Por exemplo, instalar os scripts abaixo burlará sites que limitam o acesso para não pagantes (paywall):

        https://burles.co/userscript/burlesco.user.js
        https://github.com/LegeBeker/bypass-paywalls-tampermonkey/raw/master/bypass-paywalls-tampermonkey.user.js
        https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.en.user.js
        https://gitlab.com/magnolia1234/bypass-paywalls-clean-filters/-/raw/main/userscript/bpc.es.pt.user.js
        https://possoler.tech/API/downloadExtension?filename=possoLer.user.js
    

**5. MECANISMOS DE BUSCAS COM ÊNFASE EM PRIVACIDADE:**
|   Provider   |   Search Index  | Acess to other Search engines | Encrypted Search | Open Source Code | Tor Hidden Service | Logging / Privacy Policy | Country of Operation |            Aggregated usage metrics collected           |             IP address treatment            |
| ------------ | --------------- | ----------------------------- | ---------------- | ---------------- | ------------------ | ------------------------ | -------------------- | ------------------------------------------------------- | ------------------------------------------- |
| [Brave Search](https://search.brave.com/) |   Independent   |            Google             |        No        |        Yes       |         Yes        |        Anonymized        |     United States    |                     SO and user agent                   | Temporarily processed, but are not retained |
|  [DuckDuckGo](https://duckduckgo.com/)  |       Bing      |              Bing             |        No        |         No       |         Yes        |        Anonymized	       |     United States    | Does log your searches for product improvement purposes |              Does not register              |
|   [Startpage](https://www.startpage.com/)  | Google and Bing |           Google              |        Yes       |         No       |         Yes        |        Anonymized        |      Netherlands     |                SO, user agent and language              |              Does not register              |

**6. CONFIGURAÇÕES AVANÇADAS**

Digitando about:config no campo de endereços você acessa a página de parâmetros de configurações internas do Firefox e pode fazer diversas modificações e ajustes minuciosos de aparência, segurança, funcionamento, etc.

Para criar uma nova entrada clique com o botão direito > “Nova preferência” > selecione o tipo, digite o nome da entrada e depois defina seu valor.
Existem três “tipos” de entradas:
- Boolean – aceita somente os valores true ou false, é usada ligar ou desligar uma configuração.
- Inteira – aceita qualquer número inteiro, usado para definir o valor de determinada configuração (exemplo quantidade máxima de memória usada).
- String – aceita valores compostos por letras e/ou números, usado para armazenar alguma informação (por exemplo o endereço da página inicial).

Para modificar uma entrada já existente pesquise pelo nome dela e dê dois cliques sobre ela, se for do tipo Boolean o valor será invertido, se for do tipo Inteira ou String será aberto um campo para alterar o valor.

Uma maneira mais fácil de alterar um grande número de entradas/configurações é criando um arquivo user.js (arquivo de configurações do usuário). Assim toda vez que o Firefox iniciar ele carrega as entradas presentes no arquivo e as adicionadas automaticamente às suas configurações, que ficam salvas no arquivo prefs.js.

Também é possível criar o arquivo user-overrides.js na mesma pasta, com entradas que se sobrepõem às presentes no user.js.

Para criar o arquivo de configurações faça o seguinte: Abra o Bloco de Notas, copie dentro do quadro abaixo as entradas que deseja adicionar e cole no Bloco de notas, depois salve com o nome “user.js” dentro da pasta do perfil localizada em:
“C:\Users\<Name_user>\AppData\Roaming\Mozilla\Firefox\Profiles\<something>.default”
Obs.: Para encontrar esta pasta é necessário que as pastas ocultas estejam sendo exibidas (abra o Windows Explorer => menu Ferramentas => Opções de pasta => aba Modo de exibição => marcado Mostrar pastas e arquivos ocultos).

O site [Firefox Profilemaker](https://ffprofile.com/) fornece uma maneira fácil de criar um arquivo user.js, onde basta você optar entre as diversas configurações disponíveis e ele escreve o arquivo para você baixar.

Já o repositório https://github.com/yokoffing/Betterfox oferece diversos arquivos de configurações, alguns voltados para rapidez e personalização, com destaque ao fastfox.js e ao smoothfox.js.

Porém o destaque é para o projeto ArkenFox (no repositório https://github.com/arkenfox/user.js) onde a comunidade vem aperfeiçoando constantemente um arquivo user.js com foco em privacidade e segurança, sempre se adequando às novas atualizações do Firefox. Baixe os arquivos updater.bat (para atualização) e user.js e coloque na pasta profile do Firefox indicada acima.

**CONFIRA O NOSSA ARQUIVO [USER.JS](https://gitlab.com/testa-rossa/firefox-optimization-tips/-/raw/main/user.js)** com foco em segurança e privacidade e algumas configurações úteis que melhoraram a utilização.

**VEJA ABAIXO ALGUNS EXEMPLOS DE ENTRADAS INTERESSANTES COM BREVE DESCRIÇÃO:**

    // SEGURANÇA BÁSICA
    
    // Impede sites de alterar ou ocultar os itens do navegador (como barras, tamanho da janela, etc)
    user_pref("dom.disable_window_move_resize", true);
    user_pref("dom.disable_window_open_feature.close", true);
    user_pref("dom.disable_window_open_feature.location", true);
    user_pref("dom.disable_window_open_feature.menubar", true);
    user_pref("dom.disable_window_open_feature.minimizable", true);
    user_pref("dom.disable_window_open_feature.personalbar", true);
    user_pref("dom.disable_window_open_feature.resizable", true);
    user_pref("dom.disable_window_open_feature.scrollbars", true);
    user_pref("dom.disable_window_open_feature.status", true);
    user_pref("dom.disable_window_open_feature.titlebar", true);
    user_pref("dom.disable_window_open_feature.toolbar", true);
    user_pref("dom.disable_window_showModalDialog", true);
    user_pref("dom.disable_window_status_change", true);
    user_pref("dom.disable_window_flip", true);
    
    // Links só podem abrir em nova aba na janela atual
    user_pref("browser.link.open_external", 3);
    user_pref("browser.link.open_newwindow", 3);
    user_pref("browser.link.open_newwindow.restriction", 0);
    
    // Bloquear popups iniciados por plugins (como java e flash)
    user_pref("privacy.popups.disable_from_plugins", 2);
    
    // Número máximo de redirecionamentos sucessivos
    user_pref("network.http.redirection-limit", 8);
    
    // Configurações de cookies para segurança
    user_pref("network.cookie.cookieBehavior", 4);
    user_pref("network.cookie.lifetimePolicy", 2);
    user_pref("security.cert_pinning.enforcement_level", 2);
    
    // Impede o auto recarregamento automático em sites (auto-refresh)
    user_pref("accessibility.blockautorefresh” true);
    
    // CONFIGURAÇÕES ÚTEIS
    
    // Impede sites de bloquearem o botão direito
    user_pref("dom.event.contextmenu.enabled", false);
    
    // Impedir que sites vasculhem o conteúdo da área de transferência
    user_pref("dom.event.clipboardevents.enabled", false);
    
    // Forcar instalação de addons não compatíveis
    user_pref("xpinstall.signatures.required", false);
    user_pref("extensions.checkCompatibility", false);
    
    // Mesmo valor de zoom para todos os sites, ao inves de um para cada site
    user_pref("browser.zoom.siteSpecific", false);
    
    // Ativa verificação ortográfica para textos de uma linha
    user_pref("layout.spellcheckDefault", 2);

    // Reduzir delay ao instalar add-ons
    user_pref("security.dialog_enable_delay", 0);

    // URLs completas na barra de endereços
    user_pref("browser.urlbar.trimURLs", false);

    // Define página para "página inicial"
    user_pref("browser.newtab.url", "https://duckduckgo.com/");

    // Define página para "nova guia" (obsoleto)
    user_pref("browser.startup.homepage", "https://duckduckgo.com/");

    //Desabilitar recurso autocompletar da barra de endereço
    user_pref("browser.urlbar.autocomplete.enabled", true);

    //Para mostrar o caminho completo do plugin em about:plugins
    user_pref("plugin.expose_full_path", true);

    // Não ocultar na barra de endereços o "HTTPS" ou o "WWW"
    user_pref("browser.urlbar.update1", false);
    user_pref("browser.urlbar.openViewOnFocus", false);

    // AVISOS

    // Não avisar ao fechar Abas/Guias
    user_pref("browser.tabs.warnOnClose", false);
    user_pref("browser.tabs.warnOnCloseOtherTabs", false);

    // Não avisar ao abrir about:config
    user_pref("general.warnOnAboutConfig", false);

    // Desativar checar se Firefox e o navegador principal
    user_pref("browser.shell.checkDefaultBrowser", false);

    // Sempre perguntar onde salvar
    user_pref("browser.download.useDownloadDir", false);

    // Se Livrando de pop-ups "Você realmente deseja sair desse site?"
    user_pref("dom.disable_beforeunload", true);
    
    // PARA SSD

    // Aumenta o intervalo de escrita do recovery.js para prolongar vida util do SSD
    user_pref("browser.sessionstore.interval", 300000);
    
    // Desativa o cache em disco
    user_pref("browser.cache.disk.capacity", 0);
    user_pref("browser.cache.disk.enable", false);
    user_pref("browser.cache.offline.capacity", false);
    
    // TWEAKS
    
    // Aumentar conexões (controverso)
    user_pref("network.http.max-persistent-connections-per-server", 16);
    
    // Ativar pipeling (controverso)
    user_pref("network.http.pipelining", true);
    user_pref("network.http.pipelining.abtest", false);
    user_pref("network.http.pipelining.aggressive", true);
    user_pref("network.http.pipelining.max-optimistic-requests", 3);
    user_pref("network.http.pipelining.maxrequests", 12);
    user_pref("network.http.pipelining.maxsize", 300000);
    user_pref("network.http.pipelining.read-timeout", 60000);
    user_pref("network.http.pipelining.reschedule-on-timeout", true);
    user_pref("network.http.pipelining.reschedule-timeout", 15000);
    user_pref("network.http.pipelining.ssl", true);
    user_pref("network.http.proxy.pipelining", true);

    // Reduzir tempo para exibição da página enquanto a carrega
    user_pref("content.notify.backoffcount", 5);
    user_pref("content.notify.ontimer", true);
    user_pref("nglayout.initialpaint.delay", 0);
    user_pref("ui.submenuDelay", 0);
    
    // Quantos dias um item permanece no histórico (obsoleto)
    user_pref("browser.history_expire_days", 14);
    
    // Numero maximo de sites no histórico  (obsoleto)
    user_pref("browser.history_expire_sites", 2000);
    
    // Desativar pré-carregamento automático de DNS (avaliar pros e contras)
    user_pref("network.dns.disablePrefetch", true);
    user_pref("network.dns.disablePrefetchFromHTTPS", true);
    
    // Desativar pré-carregamento automático de links (avaliar pros e contras)
    user_pref("network.prefetch-next", false);
    
    // Número de páginas na memória para o recurso Voltar/Avançar (avaliar pros e contras)
    user_pref("browser.sessionhistory.max_total_viewers", 4);
    
    // Reduz o uso de RAM quando minimizado (use somente se tiver problemas com RAM)
    user_pref("config.trim_on_minimize", true);
    
    // Definir tamanho do cache para imagens
    user_pref("browser.cache.disk.smart_size.enabled", false);
    user_pref("browser.cache.memory.capacity", 65536);

    //Tweaks genéricos
    user_pref("browser.sessionhistory.max_entries", 5);
    user_pref("browser.chrome.favicons", false);
    user_pref("browser.display.show_image_placeholders", true);
    user_pref("browser.turbo.enabled", true);
    user_pref("content.interrupt.parsing", true);
    user_pref("content.max.tokenizing.time", 2250000);
    user_pref("content.switch.threshold", 750000);
    user_pref("network.http.max-connections", 48);
    user_pref("network.http.request.max-start-delay", 0);
    
    // PRIVACIDADE E SEGURANÇA
    
    // Habilitar proteção de rastreamento
    user_pref("privacy.donottrackheader.enabled", true);
    user_pref("privacy.donottrackheader.value", 1);
    user_pref("privacy.trackingprotection.enabled", true);
    
    // Desativar Geolocalização
    user_pref("geo.enabled", false);
    user_pref("geo.wifi.logging.enabled", false);
    user_pref("geo.wifi.uri", "");

    // Desativar Telemetria
    user_pref("toolkit.telemetry.enabled", false);
    user_pref("toolkit.telemetry.unified", false);
    
    // Reforçar desativação de telemetria
    user_pref("toolkit.telemetry.archive.enabled", false);
    user_pref("toolkit.telemetry.cachedClientID", "");
    user_pref("toolkit.telemetry.previousBuildID", "");
    user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
    user_pref("toolkit.telemetry.server", "");
    
    // Desativar Segmentação Geográfica
    user_pref("browser.search.countryCode", "NOS");
    user_pref("browser.search.geoSpecificDefaults", false);
    user_pref("browser.search.geoSpecificDefaults.url", "");
    user_pref("browser.search.geoip.url", "");
    user_pref("browser.search.region", "NOS");

    // Evitar que o navegador informe aos sites sua localidade de idioma
    user_pref("intl.locale.matchOS", false);
    user_pref("general.useragent.locale", "NOS");
    
    // Desativar Relatórios de Falhas
    user_pref("breakpad.reportURL", "");
    
    // Desativar WebRTC (se usar uBlock Origin não precisa desta - pode interromper acesso à câmera e microfone como no Google Meet)
    user_pref("media.peerconnection.enabled", false);
    user_pref("media.peerconnection.use_document_iceservers", false);
    user_pref("media.navigator.enabled", false);
    user_pref("media.getusermedia.screensharing.enabled", false);
    user_pref("media.getusermedia.screensharing.allowed_domains", "");
    
    //Desativar envio de cabeçalho referer (informa site de origem)
    user_pref("network.http.sendRefererHeader", 0);
    user_pref("network.http.sendSecureXSiteReferrer", false);
    
    // Define para sempre usar TTR (Trusted Recursive Resolver) DoH (DNS over HTTPS)
    user_pref("network.trr.mode", "3");
    
    // Define o provedor TTR (Trusted Recursive Resolver) DoH (DNS over HTTPS) como Mullvad
    user_pref("network.trr.custom_uri", "https://dns.mullvad.net/dns-query");

    // Desativar envio de pings HTML5
    user_pref("browser.send_pings", false);
    user_pref("browser.send_pings.require_same_host", true);
    
    // Desativar desvio de proxy DNS
    user_pref("network.proxy.socks_remote_dns", true);
    
    // Não ser direcionado para links remotas em foco
    user_pref("network.http.speculative-parallel-limit", 0);
    
    // Punycode
    user_pref("network.IDN_show_punycode", true);
    
    // Desativar Heartbeat
    user_pref("browser.selfsupport.url", "");
    
    // Desativar Relatório de Saude
    user_pref("datareporting.healthreport.about.reportUrl", "");
    user_pref("datareporting.healthreport.about.reportUrlUnified", "");
    user_pref("datareporting.healthreport.documentServerURI", "");
    user_pref("datareporting.healthreport.infoURL", "");
    user_pref("datareporting.healthreport.logging.consoleEnabled", false);
    user_pref("datareporting.healthreport.service.enabled", false);
    user_pref("datareporting.healthreport.uploadEnabled", false);
    user_pref("datareporting.policy.dataSubmissionEnabled", false);
    user_pref("datareporting.policy.dataSubmissionEnabled.v2", false);
    
    // Desativar Navegação segura - Rastreamento Google / Log / Celular
    user_pref("browser.safebrowsing.downloads.enabled", false);
    user_pref("browser.safebrowsing.downloads.remote.enabled", false);
    user_pref("browser.safebrowsing.enabled", false);
    user_pref("browser.safebrowsing.malware.enabled", false);
    user_pref("browser.safebrowsing.provider.google.appRepURL", "");
    user_pref("browser.safebrowsing.provider.google.gethashURL", "");
    user_pref("browser.safebrowsing.provider.google.lists", "");
    user_pref("browser.safebrowsing.provider.google.reportURL", "");
    user_pref("browser.safebrowsing.provider.google.updateURL", "");
    user_pref("browser.safebrowsing.provider.mozilla.gethashURL", "");
    user_pref("browser.safebrowsing.provider.mozilla.updateURL", "");
    user_pref("browser.safebrowsing.reportMalwareMistakeURL", "");
    user_pref("browser.safebrowsing.reportPhishMistakeURL", "");
    user_pref("browser.safebrowsing.reportPhishURL", "");
    
    // Desativar WebGL (apps gráficos e jogos de navegador não funcionarão baseados em WebGL não funcionarão)
    user_pref("webgl.disabled", true);
    user_pref("webgl.disable-extensions", true);
    
    // Desativar estatísticas de Video HTML5
    user_pref("media.video_stats.enabled", false);
    
    // Desativar suporte para asm.js
    user_pref("javascript.options.asmjs", false);
    
    // Desativar verificação de metadados sobre addons instalados
    user_pref("extensions.getAddons.cache.enabled", false);
    
    // Desativar Extensões Media Encriptada - referente a DRM
    user_pref("media.eme.apiVisible", false);
    user_pref("media.eme.enabled", false);
    user_pref("browser.eme.ui.enabled", false);
    
    // DESATIVAR FUNCOES INUTEIS
    
    // Desativar Firefox Hello
    user_pref("loop.enabled", false);
    user_pref("loop.CSP", "");
    user_pref("loop.feedback.baseUrl", "");
    user_pref("loop.oauth.google.scope", "");
    user_pref("loop.server", "");
    
    // Desativar Pocket
    user_pref("extensions.pocket.enabled", false);
    user_pref("extensions.pocket.api", "");
    user_pref("extensions.pocket.oAuthConsumerKey", "");
    user_pref("extensions.pocket.site", "");
    
    // Desativar API Social do Firefox
    user_pref("social.directories", "");
    user_pref("social.remote-install.enabled", false);
    user_pref("social.share.activationPanelEnabled", false);
    user_pref("social.shareDirectory", "");
    user_pref("social.toast-notifications.enabled", false);
    user_pref("social.whitelist", "");
    
    // DESATIVAR APIs WEB INUTEIS E/OU INTRUSIVAS
    
    // Notification
    user_pref("dom.webnotifications.enabled", false);
    user_pref("dom.webnotifications.serviceworker.enabled", false);
    
    // Navigator.sendBeacon()
    user_pref("beacon.enabled", false);
    
    // WebTelephony
    user_pref("dom.telephony.enabled", false);
    user_pref("dom.vibrator.enabled", false);
    
    // Sensor API
    user_pref("device.sensors.enabled", false);
    
    // BatteryManager
    user_pref("dom.battery.enabled", false);
    user_pref("dom.cellbroadcast.enabled", false);
    
    // NavigationTimingAPI
    user_pref("dom.enable_performance", false);
    
    // Gamepad API
    user_pref("dom.gamepad.enabled", false);
    
    // Network Information API
    user_pref("dom.netinfo.enabled", false);
    
    // Changes for Web developers
    user_pref("dom.vr.enabled", false);
    user_pref("dom.vr.oculus.enabled", false);
    user_pref("dom.vr.oculus050.enabled", false);
    
    // HTML5_Speech_API
    user_pref("media.webspeech.recognition.enable", false);
    
**7. CONFIGURAÇÃO DO UBLOCK ORIGIN:**
1. Instale a partir do site [oficial](https://ublockorigin.com/) ou da loja de extensões do seu navegador.
2. Após a instalação clique sobre o ícone do uBlock com o botão direito e depois no ícone de configuração (Abrir painel).
3. Na aba "Configurações", na parte de Privacidade, marque tudo.
4. Na aba "Lista de filtros" pode marcar todas as listas de filtros, somente avalie: marcar somente Português nas regionais, não marcar as de redes sociais caso utilize e marcar mobile para uso em dispositivo móveis.
5. No fim da aba "Lista de filtros" cole o URL da lista de filtros que deseja adicionar.
**CONFIRA AS NOSSAS [LISTAS RECOMENDADAS](https://gitlab.com/testa-rossa/adblock_filter_lists)**.
